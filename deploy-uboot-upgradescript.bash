#!/bin/bash

# This script copies new MLO and u-boot.img files to a beaglebone black running 
# the 2014 Series 1 3D printer software, records the md5sum of the old files, moves
# the files and replaces them witn new ones that fix the u-boot hang bug
# see https://groups.google.com/forum/#!topic/beagleboard/mlxRz0bSHkI

# it's assumed the default user 'ubuntu' is used on the 2014 Series 1
# user will have to supply the password

if [[ "$#" -ne 1 ]] || [[ $1 == -* ]]; then
	echo "usage: $0 <hostname>"
	exit 0
fi

cd "$( dirname "${BASH_SOURCE[0]}" )"
host="$1"

# run script, providing  machine hostname 
# script sshes over, checks for exisiting mount, creates mount if it DNE, fails if it does, 
# script echos md5sum for files in mount on STDIO


ssh -t ubuntu@${host} "echo "checking files on ${host}." if test -d /tmp/bootdisk; then echo "directory on remote host already exists"; exit 0 ;else echo "creating directory"; mkdir -p /tmp/bootdisk; sudo mount -o rw /dev/mmcblk1p1 /tmp/bootdisk; md5sum /tmp/bootdisk/* | sed 's/^/${host} /g'; sudo umount /tmp/bootdisk; fi"


# since the md5sum may show the machine has unique u-boot MLO or uboot.img files,
# script prompts for user to proceed, y/n

# if n, exit
# if y, proceed
read -p "Do you want to proceed? " -n 1 -r
echo
if [[ ! $REPLY =~ ^[Yy]$ ]]
then
  exit 1
fi


# script moves MLO and u-boot.img files by appending suffix .original to each
# script copies known good MLO and u-boot.img files from client over to remote
echo "moving files and updating bootloader"
scp  MLO u-boot.img ubuntu@${host}:/tmp || exit 0

ssh -t ubuntu@${host} "sudo mv /tmp/bootdisk/MLO /tmp/bootdisk/MLO.original;sudo mv /tmp/bootdisk/u-boot.img /tmp/bootdisk/u-boot.img.original ; mv /tmp/MLO /tmp/bootdisk/MLO; mv /tmp/u-boot.img /tmp/bootdisk/u-boot.img "
#echo "rebooting your Series-1"
ssh -t ubuntu@${host} "sudo reboot now"


