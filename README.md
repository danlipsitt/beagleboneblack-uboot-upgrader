For Series 1 Beta 1, Beta 2, and early release customers:
For all customers with the 2014 Series 1 using the Beagle Bone Black

This folder contains the fix for an issue where the beagle bone black hangs unresponsive in the uboot 'hush' shell due to noise on the Beagle Bone Black's Rs232 Rx line. This bug is described extensively at https://groups.google.com/forum/#!topic/beagleboard/mlxRz0bSHkI 

You can choose to apply this fix using a plugin for the Chrome browser (beginner) or a terminal utility (advanced, recommended if you have more than one machine to fix).

Chrome browser (any OS)
-----------------------

* Install the [Chrome SSH extension][].
* Use it to connect to your printer (eg. `ubuntu@series1-b01.local`).
* Paste the contents of cutpaste.txt into the SSH window.

The wifi setup videos for [Mac][] and [Windows][] on our [video tutorials page][] include instructions on the installation and usage of the Chrome SSH extension.

[chrome ssh extension]: https://chrome.google.com/webstore/detail/secure-shell/pnhechapfaindjhompbnflcldabbghjo
[video tutorials page]: http://vimeo.com/album/2738925
[mac]: http://vimeo.com/album/2738925/video/90358292
[windows]: http://vimeo.com/album/2738925/video/90358405



Windows (PuTTY), Mac OS, Linux
------------------------------

* Open the Terminal
* `cd` into the folder containing this README.
* Run the following command:

  ```bash deployuboothangfix.bash $PRINTER```

  where `$PRINTER` is your printer's name, eg. `series1-b01.local`.
