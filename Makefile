files = README.md README.html deploy-uboot-upgradescript.bash \

VERSION = $(shell git describe)

all: zip

clean:
	-rm deploy-uboot-upgrade-script_$(VERSION).zip cutpaste.txt README.html

%.txt: %.txt.in
	m4 -P $< > $@

%.html : %.md
	markdown $< > $@

zip: deploy-uboot-upgrade-script_$(VERSION).zip

deploy-uboot-upgrade-script_$(VERSION).zip: deploy-uboot-upgrade-script_$(VERSION)

deploy-uboot-upgrade-script_$(VERSION): $(files)
	[ -d deploy-uboot-upgrade-script_$(VERSION) ] || \
mkdir deploy-uboot-upgrade-script_$(VERSION)
	cp $(files) deploy-uboot-upgrade-script_$(VERSION)


%.zip: %
	zip -r  $@ $<
